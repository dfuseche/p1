/* Generated By:JavaCC: Do not edit this line. RobotParserConstants.java */
package uniandes.lym.robot.control;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface RobotParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int T_MOVE = 4;
  /** RegularExpression Id. */
  int T_TURNRIGHT = 5;
  /** RegularExpression Id. */
  int T_PUTB = 6;
  /** RegularExpression Id. */
  int T_PICKB = 7;
  /** RegularExpression Id. */
  int T_PUTC = 8;
  /** RegularExpression Id. */
  int T_PICKC = 9;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\r\"",
    "\"\\t\"",
    "\"Move\"",
    "\"TURNRIGHT\"",
    "\"PutB\"",
    "\"PickB\"",
    "\"PutC\"",
    "\"PickC\"",
    "\"\\n\"",
  };

}
